import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    button: {
        borderRadius: 50,
        padding: 6,
        margin: 6,
        height: 60,
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5,
        shadowColor: '#171717',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        backgroundColor: '#e6f2ff',
    },
    text: {
        fontSize: 20,
        color: '#1e90ff',
    },
    buttonView: {
        width: "100%",
        marginTop: "2%",
        marginBottom: "10%",
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonLighter: {
        borderRadius: 50,
        padding: 6,
        margin: 6,
        height: 60,
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 5,
        shadowColor: '#171717',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        backgroundColor: '#f1f2f4',
    },
    confirmButton: {
        backgroundColor: '#1e90ff',
        paddingVertical: 12,
        alignItems: 'center',
        justifyContent: 'center',
    },
    confirmButtonContainer: {
        alignItems: 'center',
        marginBottom: '10%'
    }

});