import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    headerText: {
        color: '#000',
        fontWeight: "bold",
        fontSize: 20,
        marginTop: 5,
        marginBottom: 5,
    },
    normalText: {
        color: '#000',
    },
    commentText: {
        fontSize: 10,
        width: "70%",
        fontStyle: "italic",
        color: '#1e90ff',
        textAlign: "justify"
    },
    labelText: {
        width: "70%",
        textAlign: "left",
    },
    errorText: {
        fontSize: 10,
        fontStyle: "italic",
        color: '#ff0000',
        textAlign: "justify"
    },
    textInput: {
        height: 40,
        width: "70%",
        margin: 12,
        marginTop: 3,
        borderWidth: 1,
        padding: 10,
        backgroundColor: '#e6f2ff',
        color: '#000',
    },
    tableText: {
        fontSize: 20,
        color: "#fff"
    },
    totalPriceText: {
        color: '#1e90ff',
        fontWeight: "bold",
        fontSize: 20,
        textAlign: "left",
        width: "70%",
    }
})