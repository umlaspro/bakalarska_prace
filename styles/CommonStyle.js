import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container2: {
        flex: 1,
        backgroundColor: '#f2f2f2',
    },
    keyboardContainer: {
        width: "100%",
        marginBottom: "13%",
        marginTop: "5%"
    },
    headerContainer: {
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerContainer2: {
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: "10%"
    },
    datePicker: {},
    datePickerPressable: {
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    datePickerContainer: {
        marginTop: "15%",
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    errorView: {
        width: "100%",
        height: "5%",
        alignItems: 'center',
        justifyContent: 'center',
    },
    tableContainer: {
        width: "72%",
        height: "54%",
        backgroundColor: "#fff",
        marginTop: "5%"
    },
    tableContainerFiller: {
        width: "40%",
        height: "60%",
        backgroundColor: "#f2f2f2",
        position: 'absolute',
        right: 0
    },
    twoTable: {
        width: "10%",
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "7%",
        left: "8%"
    },
    twoTable2: {
        width: "10%",
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "7%",
        left: "42%"
    },
    fourTable: {
        width: "12%",
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "85%",
        left: "50%"
    },
    fourTable2: {
        width: "12%",
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "85%",
        left: "80%"
    },
    sixTable: {
        width: "24%",
        aspectRatio: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "65%",
        left: "8%"
    },
    sixTable2: {
        width: "24%",
        aspectRatio: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "85%",
        left: "8%"
    },
    eightTable: {
        width: "12%",
        aspectRatio: 1 / 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "24%",
        left: "8%"
    },
    eightTable2: {
        width: "12%",
        aspectRatio: 1 / 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1e90ff',
        position: "absolute",
        top: "24%",
        left: "40%"
    },
    menuItemContainer: {
        width: '95%',
        alignSelf: "center",
        marginVertical: 6,
        padding: 12,
        backgroundColor: '#fff',
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    menuItemText: {
        fontSize: 18,
        color: '#000',
        marginBottom: 5,
    },
    priceText: {
        fontSize: 16,
        color: '#333',
    },
    sizeText: {
        fontSize: 14,
        color: '#666',
    }, sectionHeader: {
        backgroundColor: '#e6f2ff',
        padding: 10,
    },

    sectionHeaderText: {
        fontSize: 18,
        color: '#1e90ff'
    },
    scanOverlay: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    scanText: {},
    quantityContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8,
    },
    quantityText: {
        fontSize: 16,
        marginHorizontal: 8,
    },
    addButton: {
        backgroundColor: '#1e90ff',
        padding: 8,
        borderRadius: 4,
    },
    subtractButton: {
        backgroundColor: '#ff6347',
        padding: 8,
        borderRadius: 4,
    },
    quantityButtonText: {
        color: '#fff',
        fontSize: 16,
    },
});