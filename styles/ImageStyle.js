import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    mainLogo: {
        marginTop: "7%",
        width: "85%",
        height: "50%"
    },
    smallLogo: {
        marginTop: "5%",
        width: "50%",
        height: "15%"
    },
    paymentImagesContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 16,
    },

    paymentImage: {
        width: 100,
        height: 50,
        resizeMode: 'contain',
    },
});