import HomeScreen from "./screens/HomeScreen";
import ReservationFormScreen from "./screens/ReservationFormScreen";
import ScanCodeScreen from "./screens/ScanCodeScreen";
import ViewMenuScreen from "./screens/ViewMenuScreen";
import SelectTableScreen from "./screens/SelectTableScreen";
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import OrderMenuScreen from "./screens/OrderMenuScreen";
import GetAccessToken from "./requests/GetAccessToken";
import {AppRegistry} from "react-native";

import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';
import {RestLink} from 'apollo-link-rest';
import OrderSummaryScreen from "./screens/OrderSummaryScreen";
import PaymentScreen from "./screens/PaymentScreen";
import CallWaiterScreen from "./screens/CallWaiterScreen";
import ReservationSummaryScreen from "./screens/ReservationSummaryScreen";
import PaymentSummaryScreen from "./screens/PaymentSummaryScreen";


const restLink = new RestLink({
    uri: "https://api.dotykacka.cz/v2/",
    headers: {
        Authorization: 'User a7e93223b019b60ccdf0ab00e437622e',
        'Content-Type': 'application/json',
    }
});

const client = new ApolloClient({
    uri: 'localhost:4000/graphql',
    cache: new InMemoryCache(),
    link: restLink
});

const Stack = createNativeStackNavigator();

const specificCloudId = "373657584"

export default function App() {
    return (
        <ApolloProvider client={client}>
            <GetAccessToken cloudId={specificCloudId}/>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                        name="Home"
                        component={HomeScreen}
                    />
                    <Stack.Screen
                        name="Reservation"
                        component={ReservationFormScreen}
                    />
                    <Stack.Screen
                        name="Scan code"
                        component={ScanCodeScreen}
                    />
                    <Stack.Screen
                        name="View menu"
                        component={ViewMenuScreen}
                    />
                    <Stack.Screen
                        name="Select table"
                        component={SelectTableScreen}
                    />
                    <Stack.Screen
                        name="Make an order"
                        component={OrderMenuScreen}
                    />
                    <Stack.Screen
                        name="Order made"
                        component={OrderSummaryScreen}
                        options={{
                            headerBackVisible: false,
                        }}
                    />
                    <Stack.Screen
                        name="Payment"
                        component={PaymentScreen}
                        options={{
                            headerBackVisible: false,
                        }}
                    />
                    <Stack.Screen
                        name="Waiter called"
                        component={CallWaiterScreen}
                        options={{
                            headerBackVisible: false,
                        }}
                    />
                    <Stack.Screen
                        name="Reservation created"
                        component={ReservationSummaryScreen}
                        options={{
                            headerBackVisible: false,
                        }}
                    />
                    <Stack.Screen
                        name="Order paid"
                        component={PaymentSummaryScreen}
                        options={{
                            headerBackVisible: false,
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </ApolloProvider>
    );
}

AppRegistry.registerComponent('MyApplication', () => App);