import React, {useEffect, useState, useCallback, useRef} from 'react';
import {View, Alert} from 'react-native';
import CommonStyle from '../styles/CommonStyle';
import CreateOrder from '../requests/CreateOrder';
import {StatusBar} from 'expo-status-bar';

export default function OrderSummaryScreen({route, navigation}) {
    const {scannedData, selectedItems, totalPrice} = route.params;
    const [orderStatus, setOrderStatus] = useState(null);
    const alertDisplayed = useRef(false);
    let tableId = 0;
    switch (scannedData) {
        case "royal_pork_table_1":
            tableId = 1725221326876565;
            break
        case "royal_pork_table_2":
            tableId = 640510156402574;
            break
        case "royal_pork_table_3":
            tableId = 1463567623371279;
            break
        case "royal_pork_table_4":
            tableId = 1572152987414220;
            break
        case "royal_pork_table_5":
            tableId = 3919876600688343;
            break
        case "royal_pork_table_6":
            tableId = 741965873868633;
            break
        case "royal_pork_table_7":
            tableId = 4489445098712781;
            break
        case "royal_pork_table_8":
            tableId = 2164042430475992;
            break
    }

    const orderData = {
        action: 'order/create',
        'customer-id': 0,
        'discount-percent': 0,
        'table-id': tableId,
        'user-id': 0,
        note: 'note',
        'external-id': 'tmp',
        items: selectedItems.map((item) => ({
            id: item.id,
            qty: item.quantity,
            note: 'note',
            'manual-price': item.priceWithVat,
        })),
        lock: false,
    };

    const onOrderCreatedCallback = useCallback(
        (success, orderId) => {
            if (success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert('Order Placed', 'Your order has been placed successfully. \n\n Please select if you want to pay now or later.', [
                    {text: 'Pay at the cashier', onPress: () => navigation.navigate('Home')},
                    {text: 'Pay now', onPress: () => navigation.navigate('Payment', {orderId, totalPrice})},
                ]);

                console.log('Order ID:', orderId);
            } else if (!success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert(
                    'Error',
                    'Failed to place the order. Please try again.',
                    [{text: 'OK', onPress: () => navigation.navigate('Home')}]
                );
            }
        },
        [navigation]
    );

    useEffect(() => {
        if (orderStatus) {
            onOrderCreatedCallback(orderStatus === 'success');
        }
    }, [orderStatus, onOrderCreatedCallback]);

    return (
        <View style={CommonStyle.container2}>
            {<CreateOrder orderData={orderData} onOrderCreated={onOrderCreatedCallback}/>}
            <StatusBar style="auto"/>
        </View>
    );
}
