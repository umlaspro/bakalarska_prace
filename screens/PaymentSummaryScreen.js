import React, {useEffect, useState, useCallback, useRef} from 'react';
import {View, Alert} from 'react-native';
import CommonStyle from '../styles/CommonStyle';
import {StatusBar} from 'expo-status-bar';
import PayOrder from "../requests/PayOrder";

export default function PaymentSummaryScreen({route, navigation}) {
    const {orderId} = route.params;
    const [orderStatus, setOrderStatus] = useState(null);
    const alertDisplayed = useRef(false);


    const orderData = {
        "action": "order/issue-and-pay",
        "order-id": orderId,
        "payment-method-id": 900000001
    };

    const onOrderPaidCallback = useCallback(
        (success) => {
            if (success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert('Payment was successful', 'Your payment was successful.', [
                    {text: 'OK', onPress: () => navigation.navigate('Home')},
                ]);
            } else if (!success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert(
                    'Error',
                    'Failed to place the order. Please try again.',
                    [{text: 'OK', onPress: () => navigation.navigate('Home')}]
                );
            }
        },
        [navigation]
    );


    useEffect(() => {
        if (orderStatus !== null) {
            onOrderPaidCallback(orderStatus === 'success');
        }
    }, [orderStatus, onOrderPaidCallback]);

    return (
        <View style={CommonStyle.container2}>
            {<PayOrder orderData={orderData} onOrderPaid={onOrderPaidCallback}/>}
            <StatusBar style="auto"/>
        </View>
    );
}
