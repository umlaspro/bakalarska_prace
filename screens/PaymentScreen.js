import React, {useState} from 'react';
import {StatusBar, View, TextInput, TouchableOpacity, Text, Image, Alert} from 'react-native';
import CommonStyle from '../styles/CommonStyle';
import TextStyle from '../styles/TextStyle';
import ButtonStyle from '../styles/ButtonStyle';
import ImageStyle from '../styles/ImageStyle';

import applePayImage from '../assets/apple-pay.png';
import googlePayImage from '../assets/google-pay.png';

export default function PaymentScreen({route, navigation}) {
    const {orderId, totalPrice} = route.params;

    const [cardNumber, setCardNumber] = useState('');
    const [expirationDate, setExpirationDate] = useState('');
    const [cvv, setCvv] = useState('');

    const handlePayment = () => {
        console.log('Handle Payment Function Called');

        if (cardNumber && expirationDate && cvv) {
            console.log('All Fields Filled - Payment processed successfully');
            navigation.navigate('Order paid', {orderId: orderId});
        } else {
            console.log('Incomplete Form - Showing Alert');
            Alert.alert('Incomplete Form', 'Please fill in all required fields.');
        }
    };

    const handleCancel = () => {
        Alert.alert(
            'Cancel Payment',
            'Are you sure you want to cancel the payment?\n\n You will have to pay at the cashier.',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                {
                    text: 'Yes',
                    onPress: () => {
                        console.log('Payment canceled - Navigating to Home');
                        navigation.navigate('Home');
                    },
                },
            ],
            {cancelable: false}
        );
    };

    return (
        <View style={CommonStyle.container2}>
            <View style={CommonStyle.headerContainer2}>
                <Text style={TextStyle.headerText}>Fill in payment details</Text>
            </View>
            <View style={CommonStyle.container}>
                <Text style={TextStyle.labelText}>Card Number</Text>
                <TextInput
                    style={TextStyle.textInput}
                    placeholder="Enter card number"
                    value={cardNumber}
                    onChangeText={(text) => setCardNumber(text)}
                />

                <Text style={TextStyle.labelText}>Expiration Date</Text>
                <TextInput
                    style={TextStyle.textInput}
                    placeholder="MM/YYYY"
                    value={expirationDate}
                    onChangeText={(text) => setExpirationDate(text)}
                />

                <Text style={TextStyle.labelText}>CVV</Text>
                <TextInput
                    style={TextStyle.textInput}
                    placeholder="Enter CVV"
                    value={cvv}
                    onChangeText={(text) => setCvv(text)}
                />

                <Text style={TextStyle.labelText}>Total price</Text>
                <Text style={TextStyle.totalPriceText}>{totalPrice} Kč</Text>

                <View style={ImageStyle.paymentImagesContainer}>
                    <Image source={applePayImage} style={ImageStyle.paymentImage}/>
                    <Image source={googlePayImage} style={ImageStyle.paymentImage}/>
                </View>
            </View>
            <View style={ButtonStyle.confirmButtonContainer}>
                <TouchableOpacity style={ButtonStyle.button} onPress={handlePayment}>
                    <Text style={ButtonStyle.text}>Pay Now</Text>
                </TouchableOpacity>
                <TouchableOpacity style={ButtonStyle.button} onPress={handleCancel}>
                    <Text style={ButtonStyle.text}>Cancel</Text>
                </TouchableOpacity>
            </View>

            <StatusBar style="auto"/>
        </View>
    );
}
