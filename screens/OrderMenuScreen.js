import React, {useState} from 'react';
import {Alert, SectionList, Text, TouchableOpacity, View} from 'react-native';
import CommonStyle from '../styles/CommonStyle';
import GetProducts from '../requests/GetProducts';
import ButtonStyle from '../styles/ButtonStyle';
import {StatusBar} from 'expo-status-bar';

export default function OrderMenuScreen({navigation, route}) {
    const {scannedData} = route.params;
    const [itemQuantities, setItemQuantities] = useState({});

    const calculateTotalPrice = (products) => {
        return Object.entries(itemQuantities)
            .filter(([itemId, quantity]) => quantity > 0)
            .reduce((total, [itemId, quantity]) => {
                const selectedItem = products.find((item) => item.id === itemId);
                return total + selectedItem.priceWithVat * quantity;
            }, 0);
    };
    const handleConfirmOrder = (products) => {
        const selectedItems = Object.entries(itemQuantities)
            .filter(([itemId, quantity]) => quantity > 0)
            .map(([itemId, quantity]) => {
                const selectedItem = products.find((item) => item.id === itemId);
                return {
                    id: selectedItem.id,
                    name: selectedItem.name,
                    priceWithVat: selectedItem.priceWithVat,
                    quantity: quantity,
                };
            });

        if (selectedItems.length > 0) {
            const totalPrice = calculateTotalPrice(products);

            Alert.alert(
                'Confirm Order',
                `Total Price: ${totalPrice} Kč\nMake sure that you have selected all you wanted.`,
                [
                    {
                        text: 'Cancel',
                        style: 'cancel',
                    },
                    {
                        text: 'Confirm',
                        onPress: () => {
                            console.log('Order confirmed!');
                            console.log('Scanned Data:', scannedData);
                            console.log('Total price:', totalPrice);

                            navigation.navigate('Order made', {
                                scannedData,
                                selectedItems,
                                totalPrice
                            });
                        },
                    },
                ],
                {cancelable: false}
            );
        } else {
            Alert.alert('Error', 'Total price cannot be 0. Please add items to your order.');
        }
    };

    const handleCallWaiter = () => {
        Alert.alert(
            'Call Waiter',
            'Are you sure you want to call the waiter?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                {
                    text: 'Call',
                    onPress: () => {
                        console.log("User called waiter.");
                        navigation.navigate('Waiter called', {
                            scannedData,
                        });
                    },
                },
            ],
            {cancelable: false}
        );
    };

    return (
        <View style={CommonStyle.container2}>
            <GetProducts>
                {(products) => {
                    const drinks = products.filter((item) => item._categoryId === '-201');
                    const food = products.filter((item) => item._categoryId !== '-201');

                    const sections = [
                        {title: 'Drinks', data: drinks},
                        {title: 'Food', data: food},
                    ];

                    return (
                        <View style={{flex: 1}}>
                            <SectionList
                                sections={sections}
                                keyExtractor={(item) => item.id}
                                renderSectionHeader={({section: {title}}) => (
                                    <View style={CommonStyle.sectionHeader}>
                                        <Text style={CommonStyle.sectionHeaderText}>{title}</Text>
                                    </View>
                                )}
                                renderItem={({item}) => (
                                    <View style={CommonStyle.menuItemContainer}>
                                        <Text style={CommonStyle.menuItemText}>{item.name}</Text>
                                        <Text
                                            style={CommonStyle.priceText}>{item.priceWithVat + ' Kč'} {item.currency}</Text>
                                        <Text style={CommonStyle.sizeText}>{item.description} {item.unit}</Text>

                                        <View style={CommonStyle.quantityContainer}>
                                            <TouchableOpacity
                                                style={CommonStyle.addButton}
                                                onPress={() => {
                                                    setItemQuantities((prevQuantities) => ({
                                                        ...prevQuantities,
                                                        [item.id]: (prevQuantities[item.id] || 0) + 1,
                                                    }));
                                                }}
                                            >
                                                <Text style={CommonStyle.quantityButtonText}>+</Text>
                                            </TouchableOpacity>
                                            <Text style={CommonStyle.quantityText}>{itemQuantities[item.id] || 0}</Text>
                                            <TouchableOpacity
                                                style={CommonStyle.subtractButton}
                                                onPress={() => {
                                                    setItemQuantities((prevQuantities) => ({
                                                        ...prevQuantities,
                                                        [item.id]: Math.max((prevQuantities[item.id] || 0) - 1, 0),
                                                    }));
                                                }}
                                            >
                                                <Text style={CommonStyle.quantityButtonText}>-</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )}
                            />
                            <View style={ButtonStyle.confirmButtonContainer}>
                                <TouchableOpacity
                                    style={ButtonStyle.button}
                                    onPress={() => handleConfirmOrder(products)}
                                >
                                    <Text style={ButtonStyle.text}>Confirm Order</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={ButtonStyle.button}
                                    onPress={handleCallWaiter}
                                >
                                    <Text style={ButtonStyle.text}>Call the Waiter</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    );
                }}
            </GetProducts>
            <StatusBar style="auto"/>
        </View>
    );
}
