import {StatusBar} from 'expo-status-bar';
import {SectionList, Text, View} from 'react-native';
import CommonStyle from "../styles/CommonStyle";
import GetProducts from "../requests/GetProducts";

export default function ViewMenuScreen() {
    return (
        <View style={CommonStyle.container2}>
            <GetProducts>
                {(products) => {
                    const drinks = products.filter(item => item._categoryId === '-201');
                    const food = products.filter(item => item._categoryId !== '-201');

                    const sections = [
                        {title: 'Drinks', data: drinks},
                        {title: 'Food', data: food},
                    ];

                    return (
                        <SectionList
                            sections={sections}
                            keyExtractor={(item) => item.id}
                            renderSectionHeader={({section: {title}}) => (
                                <View style={CommonStyle.sectionHeader}>
                                    <Text style={CommonStyle.sectionHeaderText}>{title}</Text>
                                </View>
                            )}
                            renderItem={({item}) => (
                                <View style={CommonStyle.menuItemContainer}>
                                    <Text style={CommonStyle.menuItemText}>{item.name}</Text>
                                    <Text
                                        style={CommonStyle.priceText}>{item.priceWithVat + " Kč"} {item.currency}</Text>
                                    <Text style={CommonStyle.sizeText}>{item.description} {item.unit}</Text>
                                </View>
                            )}
                        />
                    );
                }}
            </GetProducts>
            <StatusBar style="auto"/>
        </View>
    );
}