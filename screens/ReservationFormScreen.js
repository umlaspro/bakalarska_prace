import { useState } from "react";
import {Text, TextInput, View, Pressable, Platform, KeyboardAvoidingView, Keyboard} from 'react-native';
import { StatusBar } from 'expo-status-bar';
import DateTimePicker from "react-native-modal-datetime-picker";
import CommonStyle from "../styles/CommonStyle";
import ButtonStyle from "../styles/ButtonStyle";
import TextStyle from "../styles/TextStyle";


export default function ReservationFormScreen({ navigation }) {
    const [nameForReservation, setNameForReservation] = useState("");
    const [noteForReservation, setNoteForReservation] = useState("");

    const currDate = new Date()
    const [dateOfReservation, setDateOfReservation] = useState("");
    const [date, setDate] = useState(new Date());
    const [showPicker, setShowPicker] = useState(false);

    const [timeOfReservation, setTimeOfReservation] = useState("");
    const [time, setTime] = useState(new Date());
    const [showPickerTime, setShowPickerTime] = useState(false)

    const [errorMessage, setErrorMessage] = useState("");

    const toggleDatePicker = () => {
        setShowPicker(!showPicker);
        Keyboard.dismiss()
    };

    const toggleDatePickerTime = () => {
        setShowPickerTime(!showPickerTime);
        Keyboard.dismiss()
    };

    const handleConfirm = (date) => {
        setDate(date);
        setDateOfReservation(date.toDateString())
        toggleDatePicker();
    };

    const handleConfirmTime = (time) => {
        setTime(time);
        setTimeOfReservation(time.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}))
        toggleDatePickerTime();
    };

    const handleMinDate = () => {
        if (date.getDay() === currDate.getDay() && date.getMonth() === currDate.getMonth() && currDate.getHours() > 21){
            return new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate() + 1);
        } else {
            return new Date()
        }
    }

    const minDate = handleMinDate();

    const handleMaxDate = () => {
        return new Date(currDate.getFullYear(), currDate.getMonth() + 2, currDate.getDate());
    }

    const maxDate = handleMaxDate();

    const handleMinTime = () => {
        if(time === "") {
            setErrorMessage("Time must not be empty.");
            return false;
        }
        if (date.getDay() === currDate.getDay() && date.getMonth() === currDate.getMonth()){
            if ((time.getHours() <= currDate.getHours()) || ((time.getHours() + 1 === currDate.getHours()) && (time.getMinutes() < currDate.getMinutes()))){ //less than an hour
                setErrorMessage("The selected time must be at least one hour from now.")
                return false;
            }
        } if (time.getHours() < 9) {
            setErrorMessage("The selected time must be during opening hours. (after 9 a.m.)")
            return false;
        }
        setErrorMessage("")
        return true;
    }

    const handleMaxTime = () => {
        if ((time.getHours() > 20) || ((time.getMinutes() > 0) && (time.getHours() === 20))) {
            setErrorMessage("Reservations can only be made until 8 p.m.");
            return false;
        } else {
            setErrorMessage("")
            return true;
        }
    }

    const validateName = () => {
        if(nameForReservation === "") {
            setErrorMessage("Name must not be empty.");
            return false;
        }
        return true;
    }

    const validate = () => {
        if (validateName()){
            if (handleMinTime()) {
                if (handleMaxTime()) {
                    const reservationData = {
                        name: nameForReservation,
                        note: noteForReservation,
                        date: date.toLocaleDateString(),
                        time: time.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})
                    };
                    console.log("Selected reservation data: ", JSON.stringify(reservationData, null, 2))
                    navigation.navigate("Select table", { reservationData });
                }
            }
        }
    }

    const keyboardVerticalOffset = Platform.OS === "ios" ? 80 : -200

    return (
        <View style={CommonStyle.container}>
            <KeyboardAvoidingView
                style={CommonStyle.keyboardContainer}
                behavior='position'
                keyboardVerticalOffset={keyboardVerticalOffset}
            >
                <View style={CommonStyle.headerContainer}>
                    <Text style={TextStyle.headerText}>Fill in reservation details</Text>
                    <Text style={TextStyle.commentText}>NOTE: Reservation will be made for 2 hours.</Text>
                    <Text style={TextStyle.labelText}>*<Text style={TextStyle.commentText}> = Required fields</Text></Text>
                </View>
                <View style={CommonStyle.datePickerContainer}>
                    <Text style={TextStyle.labelText}>Your name: *</Text>
                    <TextInput
                        style={TextStyle.textInput}
                        placeholder={"Smith"}
                        onChangeText={setNameForReservation}
                        value={nameForReservation}
                        testID= {"name-input"}
                    />

                    <Text style={TextStyle.labelText}>Chose date: *</Text>
                    <DateTimePicker
                        textColor="black"
                        isVisible={showPicker}
                        mode="date"
                        display="spinner"
                        value={date}
                        style = {CommonStyle.datePicker}
                        minimumDate = {minDate}
                        maximumDate = {maxDate}
                        onConfirm = {handleConfirm}
                        onCancel= {toggleDatePicker}
                    />

                    <Pressable
                        onPress={toggleDatePicker}
                        style={CommonStyle.datePickerPressable}
                    >
                        <TextInput
                            style={TextStyle.textInput}
                            placeholder={new Date().toDateString()}
                            onChangeText={setDateOfReservation}
                            value={dateOfReservation}
                            editable={false}
                            onPressIn={toggleDatePicker}
                            testID= {"date-input"}
                        />
                    </Pressable>

                    <Text style={TextStyle.labelText}>Chose time: *</Text>

                    <DateTimePicker
                        textColor="black"
                        isVisible={showPickerTime}
                        mode="time"
                        display="spinner"
                        value={time}
                        style = {CommonStyle.datePicker}
                        onConfirm = {handleConfirmTime}
                        onCancel= {toggleDatePickerTime}
                    />

                    <Pressable
                        onPress={toggleDatePickerTime}
                        style={CommonStyle.datePickerPressable}
                    >
                        <TextInput
                            style={TextStyle.textInput}
                            placeholder={new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})}
                            onChangeText={setTimeOfReservation}
                            value={timeOfReservation}
                            editable={false}
                            onPressIn={toggleDatePickerTime}
                            testID= {"time-input"}
                        />
                    </Pressable>

                    <Text style={TextStyle.labelText}>Note:</Text>
                    <TextInput
                        style={TextStyle.textInput}
                        placeholder={""}
                        onChangeText={setNoteForReservation}
                        value={noteForReservation}
                    />
                </View>
                <View style={CommonStyle.errorView}>
                    <Text style={TextStyle.errorText}>{errorMessage}</Text>
                </View>
            </KeyboardAvoidingView>
            <View style={ButtonStyle.buttonView}>
                <Pressable
                    style={ButtonStyle.button}
                    testID= {"submit-button"}
                    onPress={validate}>
                    <Text style={ButtonStyle.text}>
                        Confirm
                    </Text>
                </Pressable>
                <Pressable
                    style={ButtonStyle.buttonLighter}
                    onPress={()=> navigation.navigate("Home")}>
                    <Text style={ButtonStyle.text}>
                        Cancel
                    </Text>
                </Pressable>
            </View>
            <StatusBar style="auto"/>
        </View>

    );
}
