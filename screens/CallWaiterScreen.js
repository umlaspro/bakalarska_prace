import {StatusBar} from 'expo-status-bar';
import {View, Alert} from 'react-native';
import {useEffect, useState, useCallback, useRef} from 'react';
import CommonStyle from '../styles/CommonStyle';
import CreateCustomer from '../requests/CreateCustomer';

export default function CallWaiterScreen({route, navigation}) {
    const {scannedData} = route.params;
    const [customerStatus, setCustomerStatus] = useState(null);
    const alertDisplayed = useRef(false);

    const getCurrentTime = () => {
        const currentTime = new Date();
        return currentTime.toLocaleTimeString();
    };

    const customerData = {
        addressLine1: 'tmp',
        barcode: 'tmp',
        companyId: 'tmp',
        companyName: 'tmp',
        deleted: false,
        display: true,
        email: '',
        firstName: getCurrentTime(),
        flags: '0',
        headerPrint: '',
        hexColor: '#CDDC39',
        internalNote: '',
        lastName: scannedData,
        phone: '',
        points: 0,
        tags: ['sample'],
        vatId: 'CZ68407700',
        zip: '',
    };

    const onCustomerCreatedCallback = useCallback(
        (success) => {
            if (success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert('Waiter called', 'Now please wait for the waiter.', [
                    {text: 'OK', onPress: () => navigation.navigate('Home')},
                ]);
            } else if (!success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert(
                    'Error',
                    'Failed to call waiter. Please try again.',
                    [{text: 'OK', onPress: () => navigation.navigate('Home')}]
                );
            }
        },
        [navigation]
    );

    useEffect(() => {
        if (customerStatus) {
            onCustomerCreatedCallback(customerStatus === 'success');
        }
    }, [customerStatus, onCustomerCreatedCallback]);

    return (
        <View style={CommonStyle.container2}>
            {customerStatus === null && (
                <CreateCustomer customerData={customerData} onCustomerCreated={onCustomerCreatedCallback}/>
            )}
            <StatusBar style="auto"/>
        </View>
    );
}
