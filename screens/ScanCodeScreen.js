import React, {useState, useEffect} from 'react';
import {StatusBar} from 'expo-status-bar';
import {BarCodeScanner} from 'expo-barcode-scanner';
import {Text, View, StyleSheet, Alert} from 'react-native';
import CommonStyle from '../styles/CommonStyle';

export default function ScanCodeScreen({navigation}) {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const {status} = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setScanned(false);
        });

        return unsubscribe;
    }, [navigation]);

    const handleBarCodeScanned = ({data}) => {
        setScanned(true);

        if (
            data === 'royal_pork_table_1' ||
            data === 'royal_pork_table_2' ||
            data === 'royal_pork_table_3' ||
            data === 'royal_pork_table_4' ||
            data === 'royal_pork_table_5' ||
            data === 'royal_pork_table_6' ||
            data === 'royal_pork_table_7' ||
            data === 'royal_pork_table_8'
        ) {
            console.log('Table QR code scanned successfully.');
            navigation.navigate('Make an order', {scannedData: data});
        } else {
            console.log('The scanned code does not match.');
            Alert.alert('Invalid Code', 'The scanned code does not match.', [
                {text: 'OK', onPress: () => setScanned(false)},
            ]);
        }
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={CommonStyle.container2}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            <View style={CommonStyle.scanOverlay}>
                <Text style={CommonStyle.scanText}>
                    {scanned ? 'Scanned successfully!' : 'Scanning...'}
                </Text>
            </View>
            <StatusBar style="auto"/>
        </View>
    );
}
