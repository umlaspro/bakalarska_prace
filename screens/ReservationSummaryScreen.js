import {StatusBar} from 'expo-status-bar';
import {View, Alert} from 'react-native';
import {useCallback, useEffect, useRef, useState} from 'react';
import CommonStyle from '../styles/CommonStyle';
import CreateReservation from "../requests/CreateReservation";

export default function ReservationSummaryScreen({route, navigation}) {
    const {
        filledReservationData,
        reservationDateTimeStart,
        reservationDateTimeEnd,
        selectedTableId,
        maxSeatCount,
    } = route.params;
    const [reservationStatus, setReservationStatus] = useState(null);
    const alertDisplayed = useRef(false);

    let {name, note} = filledReservationData;
    if (note == null) {
        note = '';
    }

    const reservationData = {
        _branchId: '123076738',
        _customerId: '0',
        _employeeId: '0',
        _tableId: selectedTableId,
        created: new Date().toISOString(),
        endDate: reservationDateTimeEnd,
        note: `Name: ${name} ${note}`,
        seats: maxSeatCount,
        startDate: reservationDateTimeStart,
        status: 'CONFIRMED',
        flags: '0',
    };

    const onReservationCreatedCallback = useCallback(
        (success) => {
            if (success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert('Reservation created', 'Reservation created successfully!.', [
                    {text: 'OK', onPress: () => navigation.navigate('Home')},
                ]);
            } else if (!success && !alertDisplayed.current) {
                alertDisplayed.current = true;
                Alert.alert(
                    'Error',
                    'Failed to create reservation. Please try again.',
                    [{text: 'OK', onPress: () => navigation.navigate('Home')}]
                );
            }
        },
        [navigation]
    );

    useEffect(() => {
        if (reservationStatus) {
            onReservationCreatedCallback(reservationStatus === 'success');
        }
    }, [reservationStatus, onReservationCreatedCallback]);

    return (
        <View style={CommonStyle.container2}>
            {reservationStatus === null && (
                <CreateReservation reservationData={reservationData}
                                   onReservationCreated={onReservationCreatedCallback}/>
            )}
            <StatusBar style="auto"/>
        </View>
    );
}
