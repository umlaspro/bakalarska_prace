import {StatusBar} from 'expo-status-bar';
import {Pressable, Text, View} from 'react-native';
import CommonStyle from '../styles/CommonStyle';
import TextStyle from '../styles/TextStyle';
import ButtonStyle from '../styles/ButtonStyle';
import {useState, useEffect} from 'react';
import GetReservations from "../requests/GetReservations";

const Table = ({tableId, selectedTableId, disabledTables, handleTablePress, mainStyle, maxSeatCount}) => {
    const isTableSelected = selectedTableId === tableId;
    const isTableDisabled = disabledTables.includes(tableId);

    return (
        <Pressable
            style={[
                mainStyle,
                isTableSelected ? {backgroundColor: 'green'} : {},
                isTableDisabled ? {backgroundColor: 'grey'} : {},
            ]}
            onPress={() => handleTablePress(tableId, maxSeatCount)}
            disabled={isTableDisabled}
        >
            <Text style={TextStyle.tableText}>{isTableDisabled ? '0' : maxSeatCount}</Text>
        </Pressable>
    );
};

export default function SelectTableScreen({navigation, route}) {
    const {reservationData} = route.params;

    const [errorMessage, setErrorMessage] = useState('');
    const [selectedTableId, setSelectedTableId] = useState(null);
    const [disabledTables, setDisabledTables] = useState([]);
    const [maxSeatCount, setMaxSeatCount] = useState(null);
    const [reservationDateTimeStart, setReservationDateTimeStart] = useState(null);
    const [reservationDateTimeEnd, setReservationDateTimeEnd] = useState(null);
    const [reservations, setReservations] = useState([]);

    const compareTimes = (reservationData, reservations) => {
        const {date, time} = reservationData;

        const [day, month, year] = date.split('.').map(Number);
        const [hours, minutes] = time.split(':').map(Number);
        const reservationDateTimeStart = new Date(Date.UTC(year, month - 1, day, hours, minutes));
        const reservationDateTimeEnd = new Date(reservationDateTimeStart);
        reservationDateTimeEnd.setUTCHours(reservationDateTimeEnd.getUTCHours() + 2);

        setReservationDateTimeStart(reservationDateTimeStart);
        setReservationDateTimeEnd(reservationDateTimeEnd);

        const conflictTables = [];

        for (const reservation of reservations) {
            const startDate = new Date(reservation.startDate);
            const endDate = new Date(reservation.endDate);
            const tableId = reservation._tableId;

            if (
                (reservationDateTimeStart >= startDate && reservationDateTimeStart < endDate) ||
                (reservationDateTimeEnd > startDate && reservationDateTimeEnd <= endDate) ||
                (reservationDateTimeStart <= startDate && reservationDateTimeEnd >= endDate)
            ) {
                console.log('Reservation time conflict');
                conflictTables.push(tableId);
            }
        }

        setDisabledTables(conflictTables);
        console.log("Tables already reserved(ids): ", conflictTables)
    };

    useEffect(() => {
        compareTimes(reservationData, reservations);
    }, [reservations, reservationData]);

    const handleTablePress = (tableId, maxSeatCount) => {
        setSelectedTableId(tableId);
        setMaxSeatCount(maxSeatCount)
        setErrorMessage('');
    };

    const validate = () => {
        if (!selectedTableId) {
            setErrorMessage("Select a table!")
            return false;
        }
        return true;
    }


    return (
        <View style={CommonStyle.container}>
            <GetReservations>
                {(reservationsData) => {
                    setReservations(reservationsData);
                    return (
                        <>
                            <Text style={TextStyle.headerText}>Select a table to reserve</Text>
                            <Text style={TextStyle.commentText}>The number represents the number of free seats.</Text>
                            <View style={CommonStyle.tableContainer}>
                                <View style={CommonStyle.tableContainerFiller}></View>
                                <Table
                                    mainStyle={CommonStyle.twoTable}
                                    tableId={"1725221326876565"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={2}
                                />
                                <Table
                                    mainStyle={CommonStyle.twoTable2}
                                    tableId={"640510156402574"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={2}
                                />
                                <Table
                                    mainStyle={CommonStyle.fourTable}
                                    tableId={"1463567623371279"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={4}
                                />
                                <Table
                                    mainStyle={CommonStyle.fourTable2}
                                    tableId={"1572152987414220"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={4}
                                />
                                <Table
                                    mainStyle={CommonStyle.sixTable}
                                    tableId={"3919876600688343"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={6}
                                />
                                <Table
                                    mainStyle={CommonStyle.sixTable2}
                                    tableId={"741965873868633"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={6}
                                />
                                <Table
                                    mainStyle={CommonStyle.eightTable}
                                    tableId={"4489445098712781"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={8}
                                />
                                <Table
                                    mainStyle={CommonStyle.eightTable2}
                                    tableId={"2164042430475992"}
                                    selectedTableId={selectedTableId}
                                    disabledTables={disabledTables}
                                    handleTablePress={handleTablePress}
                                    maxSeatCount={8}
                                />
                            </View>
                            <View style={CommonStyle.errorView}>
                                <Text style={TextStyle.errorText}>{errorMessage}</Text>
                            </View>

                            <View style={ButtonStyle.buttonView}>
                                <Pressable
                                    style={ButtonStyle.button}
                                    onPress={() => {
                                        if (validate()) {
                                            navigation.navigate('Reservation created', {
                                                filledReservationData: reservationData,
                                                reservationDateTimeStart: new Date(reservationDateTimeStart).toISOString(),
                                                reservationDateTimeEnd: new Date(reservationDateTimeEnd).toISOString(),
                                                selectedTableId: selectedTableId,
                                                maxSeatCount: maxSeatCount,
                                            });
                                        }
                                    }}
                                >
                                    <Text style={ButtonStyle.text}>Confirm</Text>
                                </Pressable>
                                <Pressable
                                    style={ButtonStyle.buttonLighter}
                                    onPress={() => navigation.navigate('Reservation')}
                                >
                                    <Text style={ButtonStyle.text}>Cancel</Text>
                                </Pressable>
                            </View>
                        </>
                    );
                }}
            </GetReservations>
            <StatusBar style="auto"/>
        </View>
    );
}