import {Pressable, View, Text, Image} from 'react-native';
import {StatusBar} from 'expo-status-bar';
import CommonStyle from "../styles/CommonStyle";
import ButtonStyle from "../styles/ButtonStyle";
import ImageStyle from "../styles/ImageStyle";

export default function HomeScreen({navigation}) {
    return (
        <View style={CommonStyle.container}>
            <Image
                style={ImageStyle.mainLogo}
                source={require("../assets/logo-no-background.png")}/>
            <View style={ButtonStyle.buttonView}>
                <Pressable
                    style={ButtonStyle.button}
                    onPress={() => navigation.navigate("Reservation")}>
                    <Text style={ButtonStyle.text}>
                        Create reservation
                    </Text>
                </Pressable>
                <Pressable
                    style={ButtonStyle.button}
                    onPress={() => navigation.navigate("Scan code")}>
                    <Text style={ButtonStyle.text}>
                        Scan QR-Code
                    </Text>
                </Pressable>
                <Pressable
                    style={ButtonStyle.button}
                    onPress={() => navigation.navigate("View menu")}>
                    <Text style={ButtonStyle.text}>
                        View menu
                    </Text>
                </Pressable>
            </View>
            <StatusBar style="auto"/>
        </View>
    );
}
