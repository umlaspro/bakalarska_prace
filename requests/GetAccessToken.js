import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {useMutation, gql, useApolloClient} from '@apollo/client';
import {RestLink} from "apollo-link-rest";

const SIGNIN_MUTATION = gql`
  mutation SignIn($cloudId: String!) {
    signin_cloudId(input: { _cloudId: $cloudId }) @rest(type: "SignInResponse", path: "signin/token", method: "POST") {
      accessToken
    }
  }
`
;

const GetAccessToken = ({cloudId}) => {
    const [accessToken, setAccessToken] = useState("");

    const client = useApolloClient();

    const [signIn] = useMutation(SIGNIN_MUTATION);

    const handleSignIn = () => {
        signIn({variables: {cloudId}})
            .then(response => {
                const {accessToken} = response.data.signin_cloudId;
                setAccessToken(accessToken);
                console.log('Access Token:', accessToken);

                const updatedRestLink = new RestLink({
                    uri: "https://api.dotykacka.cz/v2/",
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                        'Content-Type': 'application/json',
                    }
                });

                client.setLink(updatedRestLink);
            })
            .catch(error => {
            });
    };

    useEffect(() => {
        handleSignIn();
    }, []);

    return (
        <View>
        </View>
    );
};

export default GetAccessToken;
