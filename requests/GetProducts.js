import {useQuery, gql} from '@apollo/client';
import {Text, View} from 'react-native';

const PRODUCTS_QUERY = gql`
  query Products {
    products @rest(type: "ProductsList", path: "clouds/373657584/products") {
      data {
        _categoryId
        _cloudId
        _defaultCourseId
        _eetSubjectId
        _supplierId
        allergens
        alternativeName
        created
        currency
        deleted
        deliveryNoteIds
        description
        discountPercent
        discountPermitted
        display
        ean
        externalId
        features
        flags
        hexColor
        id
        imageUrl
        margin
        marginMin
        modifiedBy
        name
        notes
        onSale
        packageItem
        packaging
        packagingMeasurement
        packagingPriceWithVat
        plu
        points
        priceInPoints
        priceWithVat
        priceWithVatB
        priceWithVatC
        priceWithVatD
        priceWithVatE
        priceWithoutVat
        purchasePriceWithoutVat
        requiresPriceEntry
        sortOrder
        stockDeduct
        stockOverdraft
        subtitle
        supplierProductCode
        tags
        unit
        unitMeasurement
        vat
        versionDate
      }
    }
  }
`;

const GetProducts = ({children}) => {
    const {data, loading, error} = useQuery(PRODUCTS_QUERY);

    if (loading) {
        return (
            <View>
                <Text>Loading...</Text>
            </View>
        );
    }

    if (error) {
        console.log(error.message);
        return (
            <View>
                <Text>Error occurred while fetching products.</Text>
            </View>
        );
    }

    const products = data?.products?.data || [];

    console.log("Fetched products: ", JSON.stringify(products, null, 2));
    return children(products);
};

export default GetProducts;
