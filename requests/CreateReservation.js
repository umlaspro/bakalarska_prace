import {useMutation, gql} from '@apollo/client';
import React, {useEffect} from "react";
import {Text, View} from "react-native";
import CommonStyle from "../styles/CommonStyle";
import TextStyle from "../styles/TextStyle";

export const CREATE_RESERVATION_MUTATION = gql`
  mutation CreateReservation($reservations: [ReservationInput!]!) {
      createReservation(reservations: $reservations) @rest(
        type: "ReservationResponse"
        path: "clouds/373657584/reservations"
        method: "POST"
        bodyKey: "reservations"
      ) {
        id
      }
}
`;

const CreateReservation = ({reservationData, onReservationCreated}) => {
    const [createReservation, {loading, error}] = useMutation(CREATE_RESERVATION_MUTATION);

    useEffect(() => {
        const handleCreateReservation = async () => {
            try {
                const requestBody = JSON.stringify([reservationData]);

                await createReservation({
                    variables: {reservations: JSON.parse(requestBody)},
                });

                onReservationCreated && onReservationCreated(!error);
            } catch (error) {
                console.error('Error creating reservation:', error);
                onReservationCreated && onReservationCreated(false);
            }
        };

        handleCreateReservation();
    }, [createReservation, reservationData, onReservationCreated, error]);

    return (
        <View style={CommonStyle.container}>
            <Text style={TextStyle.headerText}>{loading ? 'Creating reservation...' : ''}</Text>
        </View>
    );
};

export default CreateReservation;
