import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {useMutation, gql} from '@apollo/client';
import CommonStyle from '../styles/CommonStyle';
import TextStyle from '../styles/TextStyle';

export const PAY_ORDER_MUTATION = gql`
  mutation PayOrder($orderInput: OrderInput!) {
    payOrder(orderInput: $orderInput) @rest(
      type: "OrderResponse"
      path: "clouds/373657584/branches/123076738/pos-actions"
      method: "POST"
      bodyKey: "orderInput"
    ) {
      id
    }
  }
`;

const PayOrder = ({orderData, onOrderPaid}) => {
    const [payOrder, {loading, error}] = useMutation(PAY_ORDER_MUTATION);

    useEffect(() => {
        const handlePayOrder = async () => {
            try {
                await payOrder({
                    variables: {orderInput: orderData},
                });

                onOrderPaid && onOrderPaid(!error);
            } catch (error) {
                console.error('Error paying order:', error);
                onOrderPaid && onOrderPaid(false);
            }
        };

        handlePayOrder();
    }, [payOrder, orderData, onOrderPaid, error]);

    return (
        <View style={CommonStyle.container}>
            <Text style={TextStyle.headerText}>{loading ? 'Paying order...' : ''}</Text>
        </View>
    );
};

export default PayOrder;
