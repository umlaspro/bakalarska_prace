import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {useMutation, gql} from '@apollo/client';
import CommonStyle from '../styles/CommonStyle';
import TextStyle from '../styles/TextStyle';

export const CREATE_ORDER_MUTATION = gql`
  mutation CreateOrder($orderInput: OrderInput!) {
    createOrder(orderInput: $orderInput) @rest(
      type: "OrderResponse"
      path: "clouds/373657584/branches/123076738/pos-actions"
      method: "POST"
      bodyKey: "orderInput"
    ) {
      order {
        id
      }
    }
  }
`;

const CreateOrder = ({orderData, onOrderCreated}) => {
    const [createOrder, {loading, error, data}] = useMutation(CREATE_ORDER_MUTATION);

    useEffect(() => {
        const handleCreateOrder = async () => {
            try {
                const result = await createOrder({
                    variables: {orderInput: orderData},
                });

                console.log(result.data.createOrder.order.id);
                const orderId = result.data.createOrder.order.id;

                onOrderCreated && onOrderCreated(!error, orderId);
            } catch (error) {
                console.error('Error creating order:', error);
                onOrderCreated && onOrderCreated(false, null);
            }
        };

        handleCreateOrder();
    }, [createOrder, orderData, onOrderCreated, error]);

    return (
        <View style={CommonStyle.container}>
            <Text style={TextStyle.headerText}>{loading ? 'Creating order...' : ''}</Text>
        </View>
    );
};

export default CreateOrder;