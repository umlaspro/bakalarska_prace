import {useQuery, gql} from '@apollo/client';
import {Text, View} from 'react-native';

export const RESERVATIONS_QUERY = gql`
  query Reservations {
    reservations @rest(type: "ReservationList", path: "clouds/373657584/reservations?sort=-created") {
      currentPage
      perPage
      totalItemsOnPage
      totalItemsCount
      firstPage
      lastPage
      nextPage
      prevPage
      data {
        _branchId
        _cloudId
        _customerId
        _employeeId
        _tableId
        created
        endDate
        flags
        id
        note
        seats
        startDate
        status
        versionDate
      }
    }
  }
`;

const GetReservation = ({children}) => {
    const {data, loading, error} = useQuery(RESERVATIONS_QUERY);

    if (loading) {
        return (
            <View>
                <Text>Loading...</Text>
            </View>
        );
    }

    if (error) {
        console.log(error.message);
        return (
            <View>
                <Text>Error occurred while fetching reservations.</Text>
            </View>
        );
    }

    const reservations = data?.reservations?.data || [];

    console.log("Fetched reservations: ", JSON.stringify(reservations, null, 2));
    return children(reservations);
};

export default GetReservation;
