import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import {useMutation, gql} from '@apollo/client';
import CommonStyle from "../styles/CommonStyle";
import TextStyle from "../styles/TextStyle";

export const CREATE_CUSTOMER_MUTATION = gql`
  mutation CreateCustomer($customers: [CustomerInput!]!) {
    createCustomer(customers: $customers) @rest(
      type: "CustomerResponse"
      path: "clouds/373657584/customers"
      method: "POST"
      bodyKey: "customers"
    ) {
      id
      firstName
      lastName
    }
  }
`;

const CreateCustomer = ({customerData, onCustomerCreated}) => {
    const [createCustomer, {loading, error}] = useMutation(CREATE_CUSTOMER_MUTATION);

    useEffect(() => {
        const handleCreateCustomer = async () => {
            try {
                const requestBody = JSON.stringify([customerData]);

                await createCustomer({
                    variables: {customers: JSON.parse(requestBody)},
                });

                onCustomerCreated && onCustomerCreated(!error);
            } catch (error) {
                console.error('Error creating customer:', error);
                onCustomerCreated && onCustomerCreated(false);
            }
        };

        handleCreateCustomer();
    }, [createCustomer, customerData, onCustomerCreated, error]);

    return (
        <View style={CommonStyle.container}>
            <Text style={TextStyle.headerText}>{loading ? 'Calling waiter...' : ''}</Text>
        </View>
    );
};

export default CreateCustomer;
