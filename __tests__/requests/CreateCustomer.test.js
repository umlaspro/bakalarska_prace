import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import { MockedProvider } from '@apollo/client/testing';
import CreateCustomer, { CREATE_CUSTOMER_MUTATION } from '../../requests/CreateCustomer'; // Adjust the path to your CreateCustomer component

const mockCustomerData = {
    firstName: "John",
    lastName: "Doe",
};

const mockCreateCustomerResponse = {
    id: '1',
    firstName: 'John',
    lastName: 'Doe',
};

const mutationMock = {
    request: {
        query: CREATE_CUSTOMER_MUTATION,
        variables: {
            customers: [mockCustomerData],
        },
    },
    result: {
        data: {
            createCustomer: mockCreateCustomerResponse,
        },
    },
};

const onCustomerCreatedMock = jest.fn();

describe('CreateCustomer', () => {
    it('calls the onCustomerCreated callback with true on successful mutation', async () => {
        const { getByText } = render(
            <MockedProvider mocks={[mutationMock]} addTypename={false}>
                <CreateCustomer customerData={mockCustomerData} onCustomerCreated={onCustomerCreatedMock} />
            </MockedProvider>
        );

        expect(getByText('Calling waiter...')).toBeTruthy();

        await waitFor(() => {
            expect(onCustomerCreatedMock).toHaveBeenCalledWith(true);
        });
    });
});

