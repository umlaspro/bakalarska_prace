import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import { MockedProvider } from '@apollo/client/testing';
import CreateOrder, { CREATE_ORDER_MUTATION } from '../../requests/CreateOrder'; // Adjust the path to your CreateOrder component

const mockOrderData = {
    order: {
        id: '1',
    },
};

const mockCreateOrderResponse = {
    order: {
        id: '1',
    },
};

const mutationMock = {
    request: {
        query: CREATE_ORDER_MUTATION,
        variables: {
            orderInput: mockOrderData,
        },
    },
    result: {
        data: {
            createOrder: mockCreateOrderResponse,
        },
    },
};

const onOrderCreatedMock = jest.fn();

describe('CreateOrder', () => {
    it('calls the onOrderCreated callback with true and order ID on successful mutation', async () => {
        const { getByText } = render(
            <MockedProvider mocks={[mutationMock]} addTypename={false}>
                <CreateOrder orderData={mockOrderData} onOrderCreated={onOrderCreatedMock} />
            </MockedProvider>
        );

        expect(getByText('Creating order...')).toBeTruthy();

        await waitFor(() => {
            expect(onOrderCreatedMock).toHaveBeenCalledWith(true, mockCreateOrderResponse.order.id);
        });
    });
});
