import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import { MockedProvider } from '@apollo/client/testing';
import PayOrder, { PAY_ORDER_MUTATION } from '../../requests/PayOrder';

const mockOrderData = {
    id: '1',
};

const mockPayOrderResponse = {
    id: '1',
};

const mutationMock = {
    request: {
        query: PAY_ORDER_MUTATION,
        variables: {
            orderInput: mockOrderData,
        },
    },
    result: {
        data: {
            payOrder: mockPayOrderResponse,
        },
    },
};

const onOrderPaidMock = jest.fn();

describe('PayOrder', () => {
    it('calls the onOrderPaid callback with true on successful mutation', async () => {
        const { getByText } = render(
            <MockedProvider mocks={[mutationMock]} addTypename={false}>
                <PayOrder orderData={mockOrderData} onOrderPaid={onOrderPaidMock} />
            </MockedProvider>
        );

        expect(getByText('Paying order...')).toBeTruthy();

        await waitFor(() => {
            expect(onOrderPaidMock).toHaveBeenCalledWith(true);
        });
    });
});
