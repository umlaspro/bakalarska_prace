import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import { MockedProvider } from '@apollo/client/testing';
import CreateReservation, { CREATE_RESERVATION_MUTATION } from '../../requests/CreateReservation'; // Adjust the path to your CreateReservation component

const mockReservationData = {
    reservationData: {
        id: '1',
    },
};

const mockCreateReservationResponse = {
    reservationDataResponse: {
        id: '1',
    },
};

const mutationMock = {
    request: {
        query: CREATE_RESERVATION_MUTATION,
        variables: {
            reservations: [mockReservationData],
        },
    },
    result: {
        data: {
            createReservation: mockCreateReservationResponse,
        },
    },
};

const onReservationCreatedMock = jest.fn();

describe('CreateReservation', () => {
    it('calls the onReservationCreated callback with true on successful mutation', async () => {
        const { getByText } = render(
            <MockedProvider mocks={[mutationMock]} addTypename={false}>
                <CreateReservation reservationData={mockReservationData} onReservationCreated={onReservationCreatedMock} />
            </MockedProvider>
        );

        expect(getByText('Creating reservation...')).toBeTruthy();

        await waitFor(() => {
            expect(onReservationCreatedMock).toHaveBeenCalledWith(true);
        });
    });
});
