import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import ReservationFormScreen from '../../screens/ReservationFormScreen';

describe('ReservationFormScreen', () => {

    it('should update the state on text input change for name', () => {
        const {getByTestId} = render(<ReservationFormScreen/>);
        const nameInput = getByTestId('name-input');

        fireEvent.changeText(nameInput, 'John Doe');
        expect(nameInput.props.value).toBe('John Doe');
    });

    it('should display an error message if the name is empty on submit', () => {
        const {getByText, getByTestId} = render(<ReservationFormScreen/>);
        const submitButton = getByTestId('submit-button');

        fireEvent.press(submitButton);
        expect(getByText('Name must not be empty.')).toBeTruthy();
    });

    it('should display the correct error message for invalid time selection', () => {
        const {getByText, getByTestId} = render(<ReservationFormScreen/>);
        const submitButton = getByTestId('submit-button');
        const nameInput = getByTestId('name-input');

        fireEvent.changeText(nameInput, 'John Doe');
        fireEvent.changeText(getByTestId('time-input'), new Date().toLocaleTimeString([], {
            hour: '2-digit',
            minute: '2-digit'
        }));
        fireEvent.changeText(getByTestId('date-input'), new Date().toDateString());
        fireEvent.press(submitButton);
        expect(getByText('The selected time must be at least one hour from now.')).toBeTruthy();
    });
});
