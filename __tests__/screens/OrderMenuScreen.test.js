import React from 'react';
import {render} from '@testing-library/react-native';
import {ApolloProvider, ApolloClient, InMemoryCache, HttpLink} from '@apollo/client';
import fetch from 'cross-fetch';
import OrderMenuScreen from '../../screens/OrderMenuScreen';

const createMockClient = () => {
    return new ApolloClient({
        link: new HttpLink({
            uri: 'https://api.dotykacka.cz/v2/',
            fetch: fetch,
        }),
        cache: new InMemoryCache(),
    });
};

const mockNavigation = {
    navigate: jest.fn(),
};

const mockRoute = {
    params: {
        productData: {
            "name": "Rizek"
        }
    },
};

describe('OrderMenuScreen', () => {
    it('renders correctly with initial state (mocked Client returns loading because it does not receive data)', () => {
        const mockClient = createMockClient();
        const {getByText} = render(
            <ApolloProvider client={mockClient}>
                <OrderMenuScreen navigation={mockNavigation} route={mockRoute}/>
            </ApolloProvider>
        );
        expect(getByText('Loading...')).toBeTruthy();
    });
});
