# Aplikace pro podporu hosta v restauraci

## O projektu
Tento repozitář obsahuje kód bakalářské práce s názvem "Aplikace pro podporu hosta v restauraci". Cílem této práce je vyvinout aplikaci, která by zlepšovala zážitek hostů v restauracích prostřednictvím moderních technologií.

## Použité technologie
- React Native
- Expo
- Apollo Client

## Instalace a spuštění
1. Klonování repozitáře:
git clone [URL repozitáře]
2. Přechod do adresáře projektu:
cd [název adresáře projektu]
3. Instalace závislostí:
npm install
4. Spuštění aplikace:
npx expo start
5. Spuštění aplikace Expo Go na mobilním zařízení a následovné spuštění aplikace
